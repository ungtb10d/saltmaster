FROM saltstack/salt:latest
LABEL maintainer="docker@ix.ai" \
      ai.ix.repository="ix.ai/saltmaster"

ARG CI_COMMIT_REF_NAME=""
ARG LATEST="false"

RUN mkdir -p /home/salt/.ssh/ /etc/salt/gpgkeys/ /etc/salt/sshkeys/ && \
    chown salt:salt /etc/salt/gpgkeys && \
    chown salt:salt /etc/salt/sshkeys && \
    chmod 700 /etc/salt/gpgkeys && \
    chmod 700 /etc/salt/sshkeys && \
    chown salt:salt /home/salt/.ssh/ && \
    apk add --no-cache openssh-client gnupg git && \
    if [ "${LATEST}" = "true" ]; then \
      pip3 --no-cache-dir install -U salt==${CI_COMMIT_REF_NAME} 'GitPython>=0.3.0'; \
    else \
      pip3 --no-cache-dir install -U salt 'GitPython>=0.3.0'; \
    fi

ADD entrypoint.sh /
ADD --chown=salt:salt ssh-config /home/salt/.ssh/config
ADD gen-scripts/* /usr/local/bin/

VOLUME ["/etc/salt/pki", "/var/cache/salt", "/etc/salt/gpgkeys", "/etc/salt/sshkeys", "/etc/salt/master.d"]

CMD ["/entrypoint.sh", "/usr/local/bin/saltinit"]

EXPOSE 4505 4506
