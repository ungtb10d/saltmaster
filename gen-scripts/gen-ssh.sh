#!/usr/bin/env sh

set -e

chmod 700 /etc/salt/sshkeys
chown -R salt:salt /etc/salt/sshkeys

if [ -f /etc/salt/sshkeys/saltmaster ]; then
  chmod 600 /etc/salt/sshkeys/saltmaster*
  echo "SSH public key already exists!"
else
  echo "WARNING: SSH Key not found. Generating..."
  ssh-keygen -q -t ecdsa -N '' -C 'salt-master@docker' -b 521 -f /etc/salt/sshkeys/saltmaster
fi

cat /etc/salt/sshkeys/saltmaster.pub
