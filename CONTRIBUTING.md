# How to contribute

* Fork the repository on [GitLab](https://gitlab.com/ix.ai/spielwiese)!
* Create a topic branch in your forked repository, based on the master branch
* Submit a [Merge Request](https://gitlab.com/ix.ai/spielwiese/-/merge_requests)
